//
//  ViewController.m
//  BookKeeping
//
//  Created by knerlington on 2015-04-16.
//  Copyright (c) 2015 Dan Lakss. All rights reserved.
//

#import "ViewController.h"
#import "EntryManager.h"
#import "Entry.h"


@interface ViewController ()

@end

@implementation ViewController

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:true];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    

    
    // Do any additional setup after loading the view, typically from a nib.
   EntryManager *manager = [EntryManager sharedInstance];
//    EntryManager *testManager = [EntryManager sharedInstance];
   [manager setupEntryList];
//    
//
//    Entry *entry = [[Entry alloc] initWith:1 andType:@"Income" andDescription:@"Salary"
//                                 andAmount:2];
//    Entry *entry2 = [[Entry alloc] initWith:1 andType:@"Income" andDescription:@"Drugs"
//                                  andAmount:2];
//    [manager addEntry:entry];
//    [manager printEntries];
//    [testManager addEntry:entry2];
//    
//    [manager printEntries];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
