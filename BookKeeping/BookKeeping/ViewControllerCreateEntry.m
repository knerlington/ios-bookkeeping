//
//  ViewControllerCreateEntry.m
//  BookKeeping
//
//  Created by knerlington on 2015-04-21.
//  Copyright (c) 2015 Dan Lakss. All rights reserved.
//

#import "ViewControllerCreateEntry.h"
#import "EntryManager.h"
#import "Entry.h"

@interface ViewControllerCreateEntry ()

//EntryManager
@property (strong) EntryManager *manager;


//Buttons
@property (weak, nonatomic) IBOutlet UIButton *button_createEntry;
@property (weak, nonatomic) IBOutlet UIButton *button_takePhoto;

//Text fields
@property (weak, nonatomic) IBOutlet UITextField *editText_description; //Entry description
@property (weak, nonatomic) IBOutlet UITextField *editText_amount; //Entry amount
//UIImageView
@property (weak, nonatomic) IBOutlet UIImageView *imageView_photo;
//UIPickerView
@property (weak, nonatomic) IBOutlet UIPickerView *pickerView;

@end

@implementation ViewControllerCreateEntry
{
    NSArray *typeList;

}

//IBActions

- (IBAction)createEntry:(id)sender {
    //Fetch description, amount and type
    NSString *desc = self.editText_description.text;
    int amount = (int) self.editText_amount.text.integerValue;
    long pickerIndex = [self.pickerView selectedRowInComponent:0];
    NSString *type = [typeList objectAtIndex:pickerIndex];
    
    //Create the entry with above values

    Entry *entry = [[Entry alloc]initWith:[self.manager.entryList count] andType: type andDescription:desc
        andAmount:amount];
    entry.image = self.imageView_photo.image;
    [self.manager addEntry:entry];
    [self.manager printEntries];

    
    
    
}

- (IBAction)takePhoto:(id)sender {
    //Create UIImagePickerController
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc]init];
    imagePicker.delegate = self;
    imagePicker.allowsEditing = YES;
    
    //Check the available camera source type
    //In this case I do not have access to a physical device and as such I can't test the camera
    //Therefore I'm setting the source type to the photo library so the user can pick an already existing photo for the entry.
    if(![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]){
        imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;

    }else{
        imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
    }

    [self presentViewController:imagePicker animated:YES completion:nil];
}

//UIImagePickerController
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    NSLog(@"media picker info: %@", info);

    self.imageView_photo.image = [info objectForKey:UIImagePickerControllerOriginalImage];
    [picker dismissViewControllerAnimated:YES completion:nil];

}
-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    [picker dismissViewControllerAnimated:YES completion:nil];
}


//UIPickerView
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}
-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return 2;
}
-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    
    return [typeList objectAtIndex:row];
}




//Resign first responders/end editing
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    //EntryManager
    self.manager = [EntryManager sharedInstance];
    typeList = [[NSArray alloc] initWithObjects:@"Income",@"Expense", nil];

    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
