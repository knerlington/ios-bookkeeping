//
//  ViewControllerDetails.m
//  BookKeeping
//
//  Created by knerlington on 2015-04-27.
//  Copyright (c) 2015 Dan Lakss. All rights reserved.
//

#import "ViewControllerDetails.h"
#import "EntryManager.h"
#import "Entry.h"

@interface ViewControllerDetails ()
//IBOutlets
@property (weak, nonatomic) IBOutlet UILabel *labelType;
@property (weak, nonatomic) IBOutlet UILabel *labelDate;
@property (weak, nonatomic) IBOutlet UILabel *labelAmount;
@property (weak, nonatomic) IBOutlet UILabel *labelDescription;
@property (weak, nonatomic) IBOutlet UIImageView *imageView_photo;


@property (nonatomic) EntryManager *manager;


@end

@implementation ViewControllerDetails

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    //Set values of the labels to the corresponding entry values through passed entry.id
    self.manager = [EntryManager sharedInstance];
    Entry *e = [self.manager fetchEntryWithId:self.id];
    self.imageView_photo.image = e.image;
    self.labelType.text = e.Type;
    self.labelAmount.text = [NSString stringWithFormat:@"%i", e.amount];
    self.labelDescription.text = [NSString stringWithFormat:@"%@", e.Description];
	//Create NSDateFormatter to shorten date
	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
	[dateFormatter setDateStyle:NSDateFormatterShortStyle];
	NSString *formattedString = [dateFormatter stringFromDate:e.date];
    self.labelDate.text = [NSString stringWithFormat:@"%@", formattedString];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
