//
//  Entry.m
//  BookKeeping
//
//  Created by knerlington on 2015-04-16.
//  Copyright (c) 2015 Dan Lakss. All rights reserved.
//

#import "Entry.h"

@implementation Entry


//Custom initializer
- (instancetype)initWith:(NSUInteger)id
    andType:(NSString*) type
    andDescription:(NSString*) description
    andAmount:(int)amount
{
    self = [super init];
    if (self) {
        //Add code here
        self.ID = id;
        self.Type = type;
        self.Description = description;
        self.amount = amount;
        self.date = [NSDate date];
        
    }
    return self;
}



@end
