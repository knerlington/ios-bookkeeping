//
//  main.m
//  BookKeeping
//
//  Created by knerlington on 2015-04-16.
//  Copyright (c) 2015 Dan Lakss. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
