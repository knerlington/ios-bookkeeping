//
//  EntryManager.m
//  BookKeeping
//
//  Created by knerlington on 2015-04-16.
//  Copyright (c) 2015 Dan Lakss. All rights reserved.
//

#import "EntryManager.h"

@implementation EntryManager


//Creates instance of class if it doesn't already exist
+(EntryManager *)sharedInstance{
    static EntryManager *inst = nil;
    
    
    if(!inst){
        inst = [[EntryManager alloc]init];

    }
    return inst;
}

//Sets up the entry list
//Call this the first time you fetch the class instance
-(void)setupEntryList{
    if(self.entryList == nil){
        self.entryList = [NSMutableArray new];

        
    }
    
}

//addEntry
-(void)addEntry:(Entry*)entry{
    [self.entryList addObject:entry];
}
//print entries
-(void)printEntries{
    NSLog(@"printEntries reached<>");
    for(int i = 0; i < [self.entryList count];i++){
        Entry *item = [self.entryList objectAtIndex:i];
        NSLog(@"Object nr %i : %@",i,item.Description);
    }
}
//fetchEntryWithId
-(Entry *)fetchEntryWithId:(int)id{
    //Returns the entry with supplied id
    
    return [self.entryList objectAtIndex:id];
}



@end
