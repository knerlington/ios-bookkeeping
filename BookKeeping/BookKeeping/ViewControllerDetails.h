//
//  ViewControllerDetails.h
//  BookKeeping
//
//  Created by knerlington on 2015-04-27.
//  Copyright (c) 2015 Dan Lakss. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewControllerDetails : UIViewController

@property (nonatomic) int id;

@end
