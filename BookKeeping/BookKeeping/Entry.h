//
//  Entry.h
//  BookKeeping
//
//  Created by knerlington on 2015-04-16.
//  Copyright (c) 2015 Dan Lakss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface Entry : NSObject

@property (nonatomic) NSUInteger ID;
@property (nonatomic) NSString *Type;
@property (nonatomic) NSString *Description;
@property (nonatomic) int amount;
@property (nonatomic) NSDate *date;


@property (nonatomic) UIImage *image;


- (instancetype)initWith:(NSUInteger)id
    andType:(NSString*) type
    andDescription:(NSString*) description
    andAmount:(int)amount;



@end
