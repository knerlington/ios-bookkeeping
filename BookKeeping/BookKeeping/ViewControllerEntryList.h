//
//  ViewControllerEntryList.h
//  BookKeeping
//
//  Created by knerlington on 2015-04-22.
//  Copyright (c) 2015 Dan Lakss. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewControllerEntryList : UIViewController <UITableViewDataSource, UITableViewDelegate>

@end
