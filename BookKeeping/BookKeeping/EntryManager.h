//
//  EntryManager.h
//  BookKeeping
//
//  Created by knerlington on 2015-04-16.
//  Copyright (c) 2015 Dan Lakss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Entry.h"

@interface EntryManager : NSObject

@property (nonatomic) NSMutableArray *entryList;

+(EntryManager*)sharedInstance; //Gets a shared instance of the class so there's only one instance being used across the entire program

-(void)setupEntryList; 
-(void)addEntry:(Entry*)entry; //Adds entries
-(void)printEntries; //Prints contents
-(Entry*)fetchEntryWithId:(int)id; //Gets specific entry with index in list
@end
