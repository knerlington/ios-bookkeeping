//
//  ViewControllerEntryList.m
//  BookKeeping
//
//  Created by knerlington on 2015-04-22.
//  Copyright (c) 2015 Dan Lakss. All rights reserved.
//

#import "ViewControllerEntryList.h"
#import "EntryManager.h"
#import "Entry.h"
#import "ViewControllerDetails.h"

@interface ViewControllerEntryList ()
@property (nonatomic) EntryManager *manager;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation ViewControllerEntryList

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    //TODO
	return [self.manager.entryList count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    //TODO
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    Entry *item = [self.manager.entryList objectAtIndex:indexPath.row];

    cell.textLabel.text = [NSString stringWithFormat:@"%@ | Amount: %i Desc: %@", item.Type, item.amount, item.Description];


    

    return cell;
}

-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath{
//    NSLog(@"Item selected with index: %ld", (long)indexPath.row);
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.manager = [EntryManager sharedInstance];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
//     Set the new view controller to connect to segue using [segue destinationViewController].
//     Pass the selected object to the new view controller.
    ViewControllerDetails *vcd = [segue destinationViewController];

	//Get index path of selected entry
    NSIndexPath *cellIndexPath = [self.tableView indexPathForSelectedRow];
    //Pass id of entry unto ViewControllerDetails.id
    Entry *e = [self.manager.entryList objectAtIndex:cellIndexPath.row];
	vcd.id = e.ID;
	
    
}


@end
